package com.khabeer.dashboard;

import android.app.Application;
import android.content.Context;

public class DashboardApp extends Application {

    private static DashboardApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Context getAppContext() {
        return instance.getApplicationContext();
    }
}
