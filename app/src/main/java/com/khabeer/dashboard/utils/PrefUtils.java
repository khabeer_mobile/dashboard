package com.khabeer.dashboard.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.khabeer.dashboard.DashboardApp;
import com.khabeer.dashboard.network.model.SessionInfo;
import com.khabeer.dashboard.network.model.User;


public class PrefUtils {
    private SharedPreferences.Editor editor;
    public SharedPreferences sharedPreferences;
    private final String LANGUAGE = "language";
    static PrefUtils prefUtils;

    private PrefUtils() {
        sharedPreferences = DashboardApp.getAppContext().getSharedPreferences("com.khabeer.dashboard", Context.MODE_PRIVATE);
    }

    public static PrefUtils getInstance() {
        if (prefUtils == null)
            prefUtils = new PrefUtils();
        return prefUtils;
    }

    public String getApiKey() {
        return sharedPreferences.getString("token", "");
    }

    public void setToken(String token) {
        editor.putString("token", token);
        editor.apply();
    }

    public void setLogin(boolean isLogin) {
        editor.putBoolean("sendVerification", isLogin);
        editor.apply();
    }

    public void setCity(String city) {
        editor.putString("city_id", city);
        editor.apply();
    }

    public boolean isLogin() {
        return sharedPreferences.getBoolean("sendVerification", false);
    }

    public void saveUserLoginData(User user) {
        if (user != null) {
            editor = sharedPreferences.edit();
            editor.putString("emp_Id", user.getEMPID());
            editor.putString("emp_ar_name", user.getEMPARNAME());
            editor.putString("emp_en_name", user.getEMPENNAME());
            editor.putString("unlimiteduser", user.getUNLIMETEDUSER());
            editor.putString("cachier", user.getCASHIER());
            editor.putString("hosp_ar_name", user.getHOSPARNAME());
            editor.putString("hosp_en_name", user.getHOSPENNAME());
            editor.putString("default_group", user.getDEFAULTGROUP());
            editor.putString("custom_username", user.getCUSTOMUSERNAME());
            editor.putString("default_branch", user.getDEFAULTBRANCH());
            editor.putString("name", user.toString());
            editor.putString("multi_branch", user.getMULTIBRANCH());
            editor.putString("master_branch", user.getMASTERBRANCH());
            editor.putString("nurseFlag", user.getDOCTORJOBTYPE());
            editor.putString("modify_pat_blood_group", user.getMODIFYPATBLOODGROUP());
            editor.putString("ORACLEDATE", user.getORACLEDATE());
            editor.apply();
        }
    }


    public User getUserLoginData() {
        User user = new User();
        user.setCASHIER(sharedPreferences.getString("cachier", ""));
        user.setEMPID(sharedPreferences.getString("emp_Id", ""));
        user.setEMPARNAME(sharedPreferences.getString("emp_ar_name", ""));
        user.setEMPENNAME(sharedPreferences.getString("emp_en_name", ""));
        user.setUNLIMETEDUSER(sharedPreferences.getString("unlimiteduser", ""));
        user.setHOSPARNAME(sharedPreferences.getString("hosp_ar_name", ""));
        user.setHOSPENNAME(sharedPreferences.getString("hosp_en_name", ""));
        user.setDEFAULTGROUP(sharedPreferences.getString("default_group", ""));
        user.setCUSTOMUSERNAME(sharedPreferences.getString("custom_username", ""));
        user.setNAME(sharedPreferences.getString("name", ""));
        user.setDEFAULTBRANCH(sharedPreferences.getString("default_branch", ""));
        user.setMULTIBRANCH(sharedPreferences.getString("multi_branch", ""));
        user.setMASTERBRANCH(sharedPreferences.getString("master_branch", ""));
        user.setMODIFYPATBLOODGROUP(sharedPreferences.getString("modify_pat_blood_group", ""));

        user.setORACLEDATE(sharedPreferences.getString("ORACLEDATE", ""));

        user.setDOCTORJOBTYPE(sharedPreferences.getString("nurseFlag", ""));
        return user;
    }


    public void firstSyncData(boolean isFirtSync) {
        editor.putBoolean("syncData", isFirtSync);
        editor.apply();
    }

    public boolean isFirstSync() {
        return sharedPreferences.getBoolean("syncData", true);
    }

    public void lastDateSyncData(String date) {
        editor.putString("lastDate", date);
        editor.apply();
    }

    public String getlastDate() {
        return sharedPreferences.getString("lastDate", "");
    }

    public void setLang(int lang) {
        editor.putInt("lang", lang);
        editor.apply();
    }

    public int getLang() {
        return sharedPreferences.getInt("lang", 1);
    }

    public void removeLogin() {
        editor = sharedPreferences.edit();
        editor.remove("id");
        editor.remove("city_id");
        editor.remove("country_id");
        setLogin(false);
        editor.apply();
    }

    public SessionInfo getSession() {
        User user = getUserLoginData();
        SessionInfo sessionInfo = new SessionInfo();
        sessionInfo.setBranchID(Integer.parseInt(user.getDEFAULTBRANCH()));
        sessionInfo.setLanguageID(getLang());
        sessionInfo.setComputerName("android");
        sessionInfo.setUserID(user.getCUSTOMUSERNAME());
        return sessionInfo;
    }


    public void saveCheckLan(boolean check) {
        editor = sharedPreferences.edit();
        editor.putBoolean("check", check);
        editor.apply();
    }


    public boolean getCheckLan() {
        return sharedPreferences.getBoolean("check", true);
    }

    public void setAppLanguage(String language) {
        editor = sharedPreferences.edit();
        editor.putString(LANGUAGE, language);
        editor.apply();
    }

    public String getAppLanguage() {
        return sharedPreferences.getString(LANGUAGE, "en");
    }

    public void setIP(String ip) {
        editor = sharedPreferences.edit();
        editor.putString("ip", ip);
        editor.apply();
    }

    public String getIP() {
        return sharedPreferences.getString("ip", "");
    }


}
