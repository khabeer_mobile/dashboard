package com.khabeer.dashboard.utils;

import android.app.Activity;
import android.app.DatePickerDialog;

import java.util.Calendar;

public class Utilities {

    public static void showDateDialog(int year, int month, int day, Activity activity, DatePickerDialog.OnDateSetListener listener) {
        Calendar calendar2 = Calendar.getInstance();
        new DatePickerDialog(activity, listener, calendar2
                .get(Calendar.YEAR), calendar2.get(Calendar.MONTH),
                calendar2.get(Calendar.DAY_OF_MONTH)).show();

    }

    public static String getDateAfterDismissDialog(int monthOfYear, int dayOfMonth, int year) {
        int month = monthOfYear + 1;
        String day, smonth;
        if (dayOfMonth <= 9) {
            day = "0" + dayOfMonth;
        } else {
            day = dayOfMonth + "";
        }
        if (month <= 9) {
            smonth = "0" + month;
        } else {
            smonth = month + "";
        }
        return day + "-" + smonth + "-" + year;
    }


}
