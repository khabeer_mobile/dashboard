package com.khabeer.dashboard.utils;

/**
 * Created by viveksb007 on 28/7/17.
 */

public class MathUtils {


    /*
    *  Maps a number from one range to another.
    * */
    public static double map(double x, double in_min, double in_max, double out_min, double out_max) {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }


}
