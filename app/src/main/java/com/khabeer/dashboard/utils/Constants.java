package com.khabeer.dashboard.utils;

public class Constants {
    public static final int permission_camera_code = 786;
    public static final int permission_write_data = 788;
    public static final int permission_gallery_read_data = 789;
    public static final int permission_storage_read_data = 790;
    public static final int GALLERY = 783, CAMERA = 784, FILE_REQUEST_CODE = 785, PDF = 786;

    public static String BASE_URL = "http://197.50.197.108:150/MobileApi/api/";

}
