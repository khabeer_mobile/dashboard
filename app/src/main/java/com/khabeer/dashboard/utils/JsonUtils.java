package com.khabeer.dashboard.utils;

import com.google.gson.JsonObject;

import org.json.JSONException;


public class JsonUtils {
    public static boolean checkValue(JsonObject jsonObject, String tag) throws JSONException {
        if (jsonObject.has(tag) && (jsonObject.get(tag).isJsonObject() || jsonObject.get(tag).isJsonArray())) {
            return true;
        }
        return false;
    }
}