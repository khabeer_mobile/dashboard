package com.khabeer.dashboard.base;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class BaseViewModel extends AndroidViewModel {
    MutableLiveData<String> mMessage;

    public BaseViewModel(@NonNull Application application) {
        super(application);
        mMessage = new MutableLiveData<>();
    }

    public void showMessage(String message) {
        mMessage.postValue(message);
    }
}
