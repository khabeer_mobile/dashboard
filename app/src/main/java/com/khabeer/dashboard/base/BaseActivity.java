package com.khabeer.dashboard.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.khabeer.dashboard.R;
import com.khabeer.dashboard.utils.NetworkUtils;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity implements BaseView {
    public V viewModel;
    public T binding;
    private Dialog mProgressDialog;

    public abstract Class<V> getViewModelClass();

    public abstract int getLayoutId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutId());
        viewModel = new ViewModelProvider(this).get(getViewModelClass());
        viewModel.mMessage.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                showMessage(s);
            }
        });
    }


    @Override
    public void showMessage(String message) {
        hideLoading();
        showSnackBar(message);
    }

    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView
                .findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, R.color.white));
        snackbar.show();
    }


    public void showLoading() {
        hideLoading();
        mProgressDialog = com.khabeer.youmeda.utils.CommonUtils.showLoadingDialog(this);
        mProgressDialog.show();
    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.cancel();
    }

    public boolean isLoading() {
        return mProgressDialog != null && mProgressDialog.isShowing();
    }

    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar(getString(R.string.some_error));
        }
    }

    public void onError(@StringRes int resId) {
        onError(getString(resId));
    }


    public void showMessage(String message, DialogInterface.OnClickListener listener) {
        if (message == null) {
            message = getString(R.string.some_error);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton(R.string.ok, listener);
        AlertDialog alert = builder.create();
        alert.setOnShowListener(dialog -> {
            Button posButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
//            posButton.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.back_selected_step_type, null));

            posButton.setTypeface(ResourcesCompat.getFont(this, R.font.din_next_regular));
            posButton.setTextColor(getResources().getColor(R.color.white));
            posButton.setTextSize(12);
        });
        alert.show();


    }


    public void showMessage(@StringRes int resId) {
        showMessage(getString(resId));
    }

    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(getApplicationContext());
    }


    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void changeStatusColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (color == R.color.white)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);


            window.setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }


}
