package com.khabeer.dashboard.base;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.kabeer.mobi_care.R;
import com.kabeer.mobi_care.utilities.ProgressHUD;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;


public abstract class BaseFragment extends Fragment implements BaseView {
    ProgressHUD progressHUD;

    @Override
    public void showLoading() {
        if (progressHUD == null)
            progressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), false, null, null);
        else
            progressHUD.show();
    }

    @Override
    public void hideLoading() {
        if (progressHUD != null)
            progressHUD.dismiss();
    }

    @Override
    public void showMessage(String message) {
        Log.d("message", message);
        showSnackBar(message);
    }


    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView
                .findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        snackbar.show();
    }


}
