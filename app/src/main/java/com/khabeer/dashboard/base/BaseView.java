package com.khabeer.dashboard.base;

/**
 * Created by KSI on 15/09/2018.
 */

public interface BaseView {
    void showLoading();

    void hideLoading();

    void showMessage(String message);
}
