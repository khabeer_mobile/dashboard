package com.khabeer.dashboard;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.khabeer.dashboard.network.DashboardServices;
import com.khabeer.dashboard.network.NetworkClient;
import com.khabeer.dashboard.network.model.BaseResponse;
import com.khabeer.dashboard.network.model.GeneralResponse.Error;
import com.khabeer.dashboard.network.model.GeneralResponse.GeneralResponseLiveData;
import com.khabeer.dashboard.network.model.SimpleKPIResponse;
import com.khabeer.dashboard.network.model.coverOutpatient4.CoverOutpatient4Response;
import com.khabeer.dashboard.network.model.coverOutpatient6.CoverOutpatient6Response;
import com.khabeer.dashboard.network.model.currentBedStatus.CurrentBedStatusResponse;
import com.khabeer.dashboard.network.model.dischargePrescCount.DischargePrescCountResponse;
import com.khabeer.dashboard.network.model.laboratoryData.LaboratoryDataResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardViewModel extends AndroidViewModel {
    DashboardServices dashboardServices;
    GeneralResponseLiveData<CoverOutpatient4Response> coverOutpatient4Response;
    GeneralResponseLiveData<CoverOutpatient6Response> coverOutpatient6Response;
    GeneralResponseLiveData<CurrentBedStatusResponse> currentBedStatusResponse;
    GeneralResponseLiveData<DischargePrescCountResponse> dischargePrescCountResponse;
    GeneralResponseLiveData<LaboratoryDataResponse> laboratoryDataResponse;
    GeneralResponseLiveData<SimpleKPIResponse> utilizationBorResponse;
    GeneralResponseLiveData<SimpleKPIResponse> mortalityMorbidityResponse;
    GeneralResponseLiveData<SimpleKPIResponse> patientCensusERResponse;
    GeneralResponseLiveData<BaseResponse> coverTurnoverGraphResponse;
    GeneralResponseLiveData<BaseResponse> coverAbsentNoGraphResponse;
    GeneralResponseLiveData<BaseResponse> coverEmployeeNoGraphResponse;
    GeneralResponseLiveData<BaseResponse> coverOutPatientComplainResponse;
    GeneralResponseLiveData<BaseResponse> coverOutPatientSatisGraphResponse;
    public DashboardViewModel(@NonNull Application application) {
        super(application);
        dashboardServices = NetworkClient.getClient().create(DashboardServices.class);
        coverOutpatient4Response = new GeneralResponseLiveData<>();
        coverOutpatient6Response = new GeneralResponseLiveData<>();
        currentBedStatusResponse = new GeneralResponseLiveData<>();
        dischargePrescCountResponse = new GeneralResponseLiveData<>();
        laboratoryDataResponse = new GeneralResponseLiveData<>();
        utilizationBorResponse = new GeneralResponseLiveData<>();
        mortalityMorbidityResponse = new GeneralResponseLiveData<>();
        patientCensusERResponse = new GeneralResponseLiveData<>();
        coverTurnoverGraphResponse = new GeneralResponseLiveData<>();
        coverAbsentNoGraphResponse = new GeneralResponseLiveData<>();
        coverEmployeeNoGraphResponse = new GeneralResponseLiveData<>();
        coverOutPatientComplainResponse = new GeneralResponseLiveData<>();
        coverOutPatientSatisGraphResponse = new GeneralResponseLiveData<>();
    }

    public void showCoverOutpatient4Response(String pDataFlag, String branchId) {
        coverOutpatient4Response.postLoading();
        dashboardServices.getCoverOutPatient4(pDataFlag, branchId).enqueue(new Callback<CoverOutpatient4Response>() {
            @Override
            public void onResponse(@NonNull Call<CoverOutpatient4Response> call, @NonNull Response<CoverOutpatient4Response> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        coverOutpatient4Response.postSuccess(response.body());
                    else
                        coverOutpatient4Response.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    coverOutpatient4Response.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<CoverOutpatient4Response> call, @NonNull Throwable t) {
                coverOutpatient4Response.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<CoverOutpatient4Response> getCoverOutpatient4Response() {
        return coverOutpatient4Response;
    }

    public void showCoverOutpatient6Response(String pDataFlag, String branchId) {
        coverOutpatient6Response.postLoading();
        dashboardServices.getCoverOutPatient6(pDataFlag, branchId).enqueue(new Callback<CoverOutpatient6Response>() {
            @Override
            public void onResponse(@NonNull Call<CoverOutpatient6Response> call, @NonNull Response<CoverOutpatient6Response> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        coverOutpatient6Response.postSuccess(response.body());
                    else
                        coverOutpatient6Response.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    coverOutpatient6Response.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<CoverOutpatient6Response> call, @NonNull Throwable t) {
                coverOutpatient6Response.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<CoverOutpatient6Response> getCoverOutpatient6Response() {
        return coverOutpatient6Response;
    }

    public void showCurrentBedStatusResponse(String pDataFlag, String branchId) {
        currentBedStatusResponse.postLoading();
        dashboardServices.getCurrentBedStatus(pDataFlag, branchId).enqueue(new Callback<CurrentBedStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<CurrentBedStatusResponse> call, @NonNull Response<CurrentBedStatusResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        currentBedStatusResponse.postSuccess(response.body());
                    else
                        currentBedStatusResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    currentBedStatusResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<CurrentBedStatusResponse> call, @NonNull Throwable t) {
                currentBedStatusResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<CurrentBedStatusResponse> getCurrentBedStatusResponse() {
        return currentBedStatusResponse;
    }

    public void showDischargePrescCountResponse(String pDataFlag, String branchId) {
        dischargePrescCountResponse.postLoading();
        dashboardServices.getDischargePrescCount(pDataFlag, branchId).enqueue(new Callback<DischargePrescCountResponse>() {
            @Override
            public void onResponse(@NonNull Call<DischargePrescCountResponse> call, @NonNull Response<DischargePrescCountResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        dischargePrescCountResponse.postSuccess(response.body());
                    else
                        dischargePrescCountResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    dischargePrescCountResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<DischargePrescCountResponse> call, @NonNull Throwable t) {
                dischargePrescCountResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<DischargePrescCountResponse> getDischargePrescCountResponse() {
        return dischargePrescCountResponse;
    }

    public void showLaboratoryDataResponse(String pDataFlag, String branchId) {
        laboratoryDataResponse.postLoading();
        dashboardServices.getLaboratoryData(pDataFlag, branchId).enqueue(new Callback<LaboratoryDataResponse>() {
            @Override
            public void onResponse(@NonNull Call<LaboratoryDataResponse> call, @NonNull Response<LaboratoryDataResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        laboratoryDataResponse.postSuccess(response.body());
                    else
                        laboratoryDataResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    laboratoryDataResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<LaboratoryDataResponse> call, @NonNull Throwable t) {
                laboratoryDataResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<LaboratoryDataResponse> getLaboratoryDataResponse() {
        return laboratoryDataResponse;
    }

    public void showUtilizationBorResponse(String pDataFlag, String branchId) {
        utilizationBorResponse.postLoading();
        dashboardServices.getUtilizationBor(pDataFlag, branchId).enqueue(new Callback<SimpleKPIResponse>() {
            @Override
            public void onResponse(@NonNull Call<SimpleKPIResponse> call, @NonNull Response<SimpleKPIResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        utilizationBorResponse.postSuccess(response.body());
                    else
                        utilizationBorResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    utilizationBorResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<SimpleKPIResponse> call, @NonNull Throwable t) {
                utilizationBorResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<SimpleKPIResponse> getUtilizationBorResponse() {
        return utilizationBorResponse;
    }

    public void showMortalityMorbidityResponse(String pDataFlag, String branchId) {
        mortalityMorbidityResponse.postLoading();
        dashboardServices.getMortalityMorbidity(pDataFlag, branchId).enqueue(new Callback<SimpleKPIResponse>() {
            @Override
            public void onResponse(@NonNull Call<SimpleKPIResponse> call, @NonNull Response<SimpleKPIResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        mortalityMorbidityResponse.postSuccess(response.body());
                    else
                        mortalityMorbidityResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    mortalityMorbidityResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<SimpleKPIResponse> call, @NonNull Throwable t) {
                mortalityMorbidityResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<SimpleKPIResponse> getMortalityMorbidityResponse() {
        return mortalityMorbidityResponse;
    }

    public void showPatientCensusERResponse(String pDataFlag, String branchId) {
        patientCensusERResponse.postLoading();
        dashboardServices.getPatientCensusER(pDataFlag, branchId).enqueue(new Callback<SimpleKPIResponse>() {
            @Override
            public void onResponse(@NonNull Call<SimpleKPIResponse> call, @NonNull Response<SimpleKPIResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        patientCensusERResponse.postSuccess(response.body());
                    else
                        patientCensusERResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    patientCensusERResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<SimpleKPIResponse> call, @NonNull Throwable t) {
                patientCensusERResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<SimpleKPIResponse> getPatientCensusERResponse() {
        return patientCensusERResponse;
    }

    public void showCoverTurnoverGraphResponse(String pDataFlag, String branchId) {
        coverTurnoverGraphResponse.postLoading();
        dashboardServices.getCoverTurnoverGraph(pDataFlag, branchId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull Response<BaseResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        coverTurnoverGraphResponse.postSuccess(response.body());
                    else
                        coverTurnoverGraphResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    coverTurnoverGraphResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                coverTurnoverGraphResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<BaseResponse> getCoverTurnoverGraphResponse() {
        return coverTurnoverGraphResponse;
    }

    public void showCoverAbsentNoGraphResponse(String pDataFlag, String branchId) {
        coverAbsentNoGraphResponse.postLoading();
        dashboardServices.getCoverAbsentNoGraph(pDataFlag, branchId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull Response<BaseResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        coverAbsentNoGraphResponse.postSuccess(response.body());
                    else
                        coverAbsentNoGraphResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    coverAbsentNoGraphResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                coverAbsentNoGraphResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<BaseResponse> getCoverAbsentNoGraphResponse() {
        return coverAbsentNoGraphResponse;
    }

    public void showCoverEmployeeNoGraphResponse(String pDataFlag, String branchId) {
        coverEmployeeNoGraphResponse.postLoading();
        dashboardServices.getCoverEmployeeNoGraph(pDataFlag, branchId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull Response<BaseResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        coverEmployeeNoGraphResponse.postSuccess(response.body());
                    else
                        coverEmployeeNoGraphResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    coverEmployeeNoGraphResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                coverEmployeeNoGraphResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<BaseResponse> getCoverEmployeeNoGraphResponse() {
        return coverEmployeeNoGraphResponse;
    }

    public void showCoverOutPatientComplainResponse(String pDataFlag, String branchId) {
        coverOutPatientComplainResponse.postLoading();
        dashboardServices.getCoverOutPatientComplain(pDataFlag, branchId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull Response<BaseResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        coverOutPatientComplainResponse.postSuccess(response.body());
                    else
                        coverOutPatientComplainResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    coverOutPatientComplainResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                coverOutPatientComplainResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<BaseResponse> getCoverOutPatientComplainResponse() {
        return coverOutPatientComplainResponse;
    }

    public void showCoverOutPatientSatisGraphResponse(String pDataFlag, String branchId) {
        coverOutPatientSatisGraphResponse.postLoading();
        dashboardServices.getCoverOutPatientSatisGraph(pDataFlag, branchId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull Response<BaseResponse> response) {
                if(response.isSuccessful() && response.body() != null) {
                    if(response.body().getMessage() == null)
                        coverOutPatientSatisGraphResponse.postSuccess(response.body());
                    else
                        coverOutPatientSatisGraphResponse.postError(new Error(response.body().getMessage(), response.body().getMessage(), response.code(), null));
                }
                else
                    coverOutPatientSatisGraphResponse.postError(new Error(response.message(), response.message(), response.code(), null));
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                coverOutPatientSatisGraphResponse.postError(new Error(t.getLocalizedMessage(), t.getLocalizedMessage(), null, t));
            }
        });
    }

    public GeneralResponseLiveData<BaseResponse> getCoverOutPatientSatisGraphResponse() {
        return coverOutPatientSatisGraphResponse;
    }
}
