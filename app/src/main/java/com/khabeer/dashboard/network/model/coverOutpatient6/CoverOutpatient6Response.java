package com.khabeer.dashboard.network.model.coverOutpatient6;

import com.google.gson.annotations.SerializedName;
import com.khabeer.dashboard.network.model.BaseResponse;

public class CoverOutpatient6Response extends BaseResponse {

	@SerializedName("OUTPATIENT_CANCELATIONS")
	private String outPatientCancellations;

	public String getOutPatientCancellations() {
		return outPatientCancellations;
	}
}