package com.khabeer.dashboard.network.model.GeneralResponse;

import androidx.annotation.NonNull;

import com.khabeer.dashboard.utils.PrefUtils;

public class Error {

    private String arMsg, enMsg;
    private Integer code;
    private Throwable throwable;

    public Error(String arMsg, String enMsg, Integer code, Throwable throwable) {
        this.arMsg = arMsg;
        this.enMsg = enMsg;
        this.code = code;
        this.throwable = throwable;
    }

    public String getArMsg() {
        return arMsg;
    }

    public void setArMsg(String arMsg) {
        this.arMsg = arMsg;
    }

    public String getEnMsg() {
        return enMsg;
    }

    public void setEnMsg(String enMsg) {
        this.enMsg = enMsg;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @NonNull
    @Override
    public String toString() {
        return PrefUtils.getInstance().getAppLanguage().equals("ar") ? getArMsg() : getEnMsg();
    }
}
