package com.khabeer.dashboard.network;

import com.khabeer.dashboard.network.model.BaseResponse;
import com.khabeer.dashboard.network.model.SimpleKPIResponse;
import com.khabeer.dashboard.network.model.coverOutpatient4.CoverOutpatient4Response;
import com.khabeer.dashboard.network.model.coverOutpatient6.CoverOutpatient6Response;
import com.khabeer.dashboard.network.model.currentBedStatus.CurrentBedStatusResponse;
import com.khabeer.dashboard.network.model.dischargePrescCount.DischargePrescCountResponse;
import com.khabeer.dashboard.network.model.laboratoryData.LaboratoryDataResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DashboardServices {
    @GET("api/NewDashBoard/UTILIZATION_BOR")
    Call<SimpleKPIResponse> getUtilizationBor(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/CURRENT_BED_STATUS")
    Call<CurrentBedStatusResponse> getCurrentBedStatus(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/Mortality_Morbidity_Mortality")
    Call<SimpleKPIResponse> getMortalityMorbidity(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/KPI_COVER_OUTPATIENT_6")
    Call<CoverOutpatient6Response> getCoverOutPatient6(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/GET_DISCHARGE_PRESC_COUNTS")
    Call<DischargePrescCountResponse> getDischargePrescCount(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/KPI_COVER_OUTPATIENT_4")
    Call<CoverOutpatient4Response> getCoverOutPatient4(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/Get_Laboratory_Data")
    Call<LaboratoryDataResponse> getLaboratoryData(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/cover_turnover_graph")
    Call<BaseResponse> getCoverTurnoverGraph(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/cover_absent_no_graph")
    Call<BaseResponse> getCoverAbsentNoGraph(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/PATIENT_CENSUS_ER")
    Call<SimpleKPIResponse> getPatientCensusER(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/cover_employee_no_graph")
    Call<BaseResponse> getCoverEmployeeNoGraph(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/cover_outpt_complain")
    Call<BaseResponse> getCoverOutPatientComplain(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
    @GET("api/NewDashBoard/cover_outpat_satis_graph")
    Call<BaseResponse> getCoverOutPatientSatisGraph(@Query("P_DATE_FLAG") String pDataFlag, @Query("branch_id") String branchId);
}
