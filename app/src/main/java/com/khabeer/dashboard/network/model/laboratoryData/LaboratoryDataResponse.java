package com.khabeer.dashboard.network.model.laboratoryData;

import com.google.gson.annotations.SerializedName;
import com.khabeer.dashboard.network.model.BaseResponse;

public class LaboratoryDataResponse extends BaseResponse {

	@SerializedName("RECEIVED_ORDERS")
	private String receivedOrders;

	@SerializedName("CONFIRM_WRITE_AVG_H")
	private String confirmWriteAvgH;

	@SerializedName("WRITE_RECIVE_AVG_M")
	private String confirmWReceiveAvgHM;

	@SerializedName("WRITE_RECIVE_AVG_H")
	private String writeReceiveAvgH;

	@SerializedName("CONFIRMED_ORDERS")
	private String confirmedOrders;

	@SerializedName("PENDING_ORDERS")
	private String pendingOrders;

	@SerializedName("RECIVE_SAMPLE_AVG_H")
	private String receiveSampleAvgH;

	@SerializedName("RECIVE_SAMPLE_AVG_M")
	private String receiveSampleAvgM;

	@SerializedName("SAMPLE_REQ_AVG_M")
	private String sampleReqAvgM;

	@SerializedName("CONFIRM_RECIVE_AVG_M")
	private String confirmRecieveAvgM;

	@SerializedName("SAMPLEDED_ORDERS")
	private String sampledOrders;

	@SerializedName("CONFIRM_WRITE_AVG_M")
	private String getConfirmWriteAvgH;

	@SerializedName("WRITED_ORDERS")
	private String writtenOrders;

	@SerializedName("SAMPLE_REQ_AVG_H")
	private String sampleReqAvgH;

	@SerializedName("CONFIRM_RECIVE_AVG_H")
	private String confirmReceiveAvgH;

	public String getReceivedOrders() {
		return receivedOrders;
	}

	public String getConfirmWriteAvgH() {
		return confirmWriteAvgH;
	}

	public String getConfirmWReceiveAvgHM() {
		return confirmWReceiveAvgHM;
	}

	public String getWriteReceiveAvgH() {
		return writeReceiveAvgH;
	}

	public String getConfirmedOrders() {
		return confirmedOrders;
	}

	public String getPendingOrders() {
		return pendingOrders;
	}

	public String getReceiveSampleAvgH() {
		return receiveSampleAvgH;
	}

	public String getReceiveSampleAvgM() {
		return receiveSampleAvgM;
	}

	public String getSampleReqAvgM() {
		return sampleReqAvgM;
	}

	public String getConfirmRecieveAvgM() {
		return confirmRecieveAvgM;
	}

	public String getSampledOrders() {
		return sampledOrders;
	}

	public String getGetConfirmWriteAvgH() {
		return getConfirmWriteAvgH;
	}

	public String getWrittenOrders() {
		return writtenOrders;
	}

	public String getSampleReqAvgH() {
		return sampleReqAvgH;
	}

	public String getConfirmReceiveAvgH() {
		return confirmReceiveAvgH;
	}
}