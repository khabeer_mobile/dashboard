package com.khabeer.dashboard.network.model.coverOutpatient4;

import com.google.gson.annotations.SerializedName;
import com.khabeer.dashboard.network.model.BaseResponse;

public class CoverOutpatient4Response extends BaseResponse {

	@SerializedName("OUTPATIENT_NEW_COUNT")
	private String outPatientNewCount;

	public String getOutPatientNewCount(){
		return outPatientNewCount;
	}
}