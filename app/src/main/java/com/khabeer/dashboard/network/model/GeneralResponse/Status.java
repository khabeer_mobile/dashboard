package com.khabeer.dashboard.network.model.GeneralResponse;

public enum Status {
    Loading,
    Success,
    Failure
}
