package com.khabeer.dashboard.network.model.currentBedStatus;

import com.google.gson.annotations.SerializedName;
import com.khabeer.dashboard.network.model.BaseResponse;

public class CurrentBedStatusResponse extends BaseResponse {

	@SerializedName("CARE_BED_NOT_AVAILABLE")
	private String careBedNotAvailable;

	@SerializedName("CARE_BED_AVAILABLE")
	private String careBedAvailable;

	@SerializedName("FLOOR_BED_AVAILABLE")
	private String floorBedAvailable;

	@SerializedName("FLOOR_BED_NOT_AVAILABLE")
	private String floorBedNotAvailable;

	public String getCareBedAvailable() {
		return careBedAvailable;
	}

	public String getCareBedNotAvailable() {
		return careBedNotAvailable;
	}

	public String getFloorBedAvailable() {
		return floorBedAvailable;
	}

	public String getFloorBedNotAvailable() {
		return floorBedNotAvailable;
	}
}