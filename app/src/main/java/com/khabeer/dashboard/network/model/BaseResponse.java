package com.khabeer.dashboard.network.model;

import com.google.gson.annotations.SerializedName;

public class BaseResponse{

    @SerializedName("Message")
    private String message;

    public String getMessage(){
        return message;
    }
}
