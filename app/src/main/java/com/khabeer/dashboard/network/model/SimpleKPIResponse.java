package com.khabeer.dashboard.network.model;

import com.google.gson.annotations.SerializedName;

public class SimpleKPIResponse extends BaseResponse {

    @SerializedName("KPI_VALUE")
    private String value;

    public String getValue(){
        return value;
    }
}
