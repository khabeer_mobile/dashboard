package com.khabeer.dashboard.network.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.khabeer.dashboard.utils.PrefUtils;

public class User {
    @SerializedName("NAME_AR")
    private String mNAMEAR;

    public String getNAMEAR() {
        return mNAMEAR;
    }

    public String getNAMEEN() {
        return mNAMEEN;
    }

    @SerializedName("NAME_EN")
    private String mNAMEEN;
    @SerializedName("CASHIER")
    private String mCASHIER;
    @SerializedName("CUSTOM_USERNAME")
    private String mCUSTOMUSERNAME;
    @SerializedName("DEFAULTBRANCH")
    private String mDEFAULTBRANCH;
    @SerializedName("DEFAULTGROUP")
    private String mDEFAULTGROUP;
    @SerializedName("DEFAULTLANGUAGE")
    private String mDEFAULTLANGUAGE;
    @SerializedName("DEFAULTPAGE")
    private String mDEFAULTPAGE;
    @SerializedName("DISPENCE_ON_CLOSED_OPERATION")
    private String mDISPENCEONCLOSEDOPERATION;
    @SerializedName("DOCTOR_JOB_TYPE")
    private String mDOCTORJOBTYPE;
    @SerializedName("EMP_AR_NAME")
    private String mEMPARNAME;
    @SerializedName("EMP_EMAIL_ADDRESS")
    private String mEMPEMAILADDRESS;
    @SerializedName("EMP_EN_NAME")
    private String mEMPENNAME;
    @SerializedName("EMPID")
    private String mEMPID;
    @SerializedName("HAS_BRANCHES_OR_GROUPS")
    private String mHASBRANCHESORGROUPS;
    @SerializedName("HOSP_AR_NAME")
    private String mHOSPARNAME;
    @SerializedName("HOSP_EN_NAME")
    private String mHOSPENNAME;
    @SerializedName("MAIN_SCREEN_VIEW")
    private String mMAINSCREENVIEW;
    @SerializedName("MASTER_BRANCH")
    private String mMASTERBRANCH;
    @SerializedName("MODIFY_PAT_BLOOD_GROUP")
    private String mMODIFYPATBLOODGROUP;
    @SerializedName("MULTIBRANCH")
    private String mMULTIBRANCH;
    @SerializedName("NAME")
    private String mNAME;
    @SerializedName("OFFICER_HOSPITAL_CODE")
    private String mOFFICERHOSPITALCODE;
    @SerializedName("OFFICER_INTEGRATION")
    private String mOFFICERINTEGRATION;
    @SerializedName("ORACLEDATE")
    private String mORACLEDATE;
    @SerializedName("RESTRICTED_LINK")
    private String mRESTRICTEDLINK;
    @SerializedName("SCANNING_FILING_METHOD")
    private String mSCANNINGFILINGMETHOD;
    @SerializedName("SCANNING_STORAGE_PATH")
    private String mSCANNINGSTORAGEPATH;
    @SerializedName("SCAN_TECH_ID")
    private String mSCANTECHID;
    @SerializedName("UNLIMETEDUSER")
    private String mUNLIMETEDUSER;

    public String getCASHIER() {
        return mCASHIER;
    }

    public void setCASHIER(String cASHIER) {
        mCASHIER = cASHIER;
    }

    public String getCUSTOMUSERNAME() {
        return mCUSTOMUSERNAME;
    }

    public void setCUSTOMUSERNAME(String cUSTOMUSERNAME) {
        mCUSTOMUSERNAME = cUSTOMUSERNAME;
    }

    public String getDEFAULTBRANCH() {
        return mDEFAULTBRANCH;
    }

    public void setDEFAULTBRANCH(String dEFAULTBRANCH) {
        mDEFAULTBRANCH = dEFAULTBRANCH;
    }

    public String getDEFAULTGROUP() {
        return mDEFAULTGROUP;
    }

    public void setDEFAULTGROUP(String dEFAULTGROUP) {
        mDEFAULTGROUP = dEFAULTGROUP;
    }

    public String getDEFAULTLANGUAGE() {
        return mDEFAULTLANGUAGE;
    }

    public void setDEFAULTLANGUAGE(String dEFAULTLANGUAGE) {
        mDEFAULTLANGUAGE = dEFAULTLANGUAGE;
    }

    public String getDEFAULTPAGE() {
        return mDEFAULTPAGE;
    }

    public void setDEFAULTPAGE(String dEFAULTPAGE) {
        mDEFAULTPAGE = dEFAULTPAGE;
    }

    public String getDISPENCEONCLOSEDOPERATION() {
        return mDISPENCEONCLOSEDOPERATION;
    }

    public void setDISPENCEONCLOSEDOPERATION(String dISPENCEONCLOSEDOPERATION) {
        mDISPENCEONCLOSEDOPERATION = dISPENCEONCLOSEDOPERATION;
    }

    public String getDOCTORJOBTYPE() {
        return mDOCTORJOBTYPE;
    }

    public void setDOCTORJOBTYPE(String dOCTORJOBTYPE) {
        mDOCTORJOBTYPE = dOCTORJOBTYPE;
    }

    public String getEMPARNAME() {
        return mEMPARNAME;
    }

    public void setEMPARNAME(String eMPARNAME) {
        mEMPARNAME = eMPARNAME;
    }

    public String getEMPEMAILADDRESS() {
        return mEMPEMAILADDRESS;
    }

    public void setEMPEMAILADDRESS(String eMPEMAILADDRESS) {
        mEMPEMAILADDRESS = eMPEMAILADDRESS;
    }

    public String getEMPENNAME() {
        return mEMPENNAME;
    }

    public void setEMPENNAME(String eMPENNAME) {
        mEMPENNAME = eMPENNAME;
    }

    public String getEMPID() {
        return mEMPID;
    }

    public void setEMPID(String eMPID) {
        mEMPID = eMPID;
    }

    public String getHASBRANCHESORGROUPS() {
        return mHASBRANCHESORGROUPS;
    }

    public void setHASBRANCHESORGROUPS(String hASBRANCHESORGROUPS) {
        mHASBRANCHESORGROUPS = hASBRANCHESORGROUPS;
    }

    public String getHOSPARNAME() {
        return mHOSPARNAME;
    }

    public void setHOSPARNAME(String hOSPARNAME) {
        mHOSPARNAME = hOSPARNAME;
    }

    public String getHOSPENNAME() {
        return mHOSPENNAME;
    }

    public void setHOSPENNAME(String hOSPENNAME) {
        mHOSPENNAME = hOSPENNAME;
    }

    public String getMAINSCREENVIEW() {
        return mMAINSCREENVIEW;
    }

    public void setMAINSCREENVIEW(String mAINSCREENVIEW) {
        mMAINSCREENVIEW = mAINSCREENVIEW;
    }

    public String getMASTERBRANCH() {
        return mMASTERBRANCH;
    }

    public void setMASTERBRANCH(String mASTERBRANCH) {
        mMASTERBRANCH = mASTERBRANCH;
    }

    public String getMODIFYPATBLOODGROUP() {
        return mMODIFYPATBLOODGROUP;
    }

    public void setMODIFYPATBLOODGROUP(String mODIFYPATBLOODGROUP) {
        mMODIFYPATBLOODGROUP = mODIFYPATBLOODGROUP;
    }

    public String getMULTIBRANCH() {
        return mMULTIBRANCH;
    }

    public void setMULTIBRANCH(String mULTIBRANCH) {
        mMULTIBRANCH = mULTIBRANCH;
    }

    public String getNAME() {
        return mNAME;
    }

    public void setNAME(String nAME) {
        mNAME = nAME;
    }

    public String getOFFICERHOSPITALCODE() {
        return mOFFICERHOSPITALCODE;
    }

    public void setOFFICERHOSPITALCODE(String oFFICERHOSPITALCODE) {
        mOFFICERHOSPITALCODE = oFFICERHOSPITALCODE;
    }

    public String getOFFICERINTEGRATION() {
        return mOFFICERINTEGRATION;
    }

    public void setOFFICERINTEGRATION(String oFFICERINTEGRATION) {
        mOFFICERINTEGRATION = oFFICERINTEGRATION;
    }

    public String getORACLEDATE() {
        return mORACLEDATE;
    }

    public void setORACLEDATE(String oRACLEDATE) {
        mORACLEDATE = oRACLEDATE;
    }

    public String getRESTRICTEDLINK() {
        return mRESTRICTEDLINK;
    }

    public void setRESTRICTEDLINK(String rESTRICTEDLINK) {
        mRESTRICTEDLINK = rESTRICTEDLINK;
    }

    public String getSCANNINGFILINGMETHOD() {
        return mSCANNINGFILINGMETHOD;
    }

    public void setSCANNINGFILINGMETHOD(String sCANNINGFILINGMETHOD) {
        mSCANNINGFILINGMETHOD = sCANNINGFILINGMETHOD;
    }

    public String getSCANNINGSTORAGEPATH() {
        return mSCANNINGSTORAGEPATH;
    }

    public void setSCANNINGSTORAGEPATH(String sCANNINGSTORAGEPATH) {
        mSCANNINGSTORAGEPATH = sCANNINGSTORAGEPATH;
    }

    public String getSCANTECHID() {
        return mSCANTECHID;
    }

    public void setSCANTECHID(String sCANTECHID) {
        mSCANTECHID = sCANTECHID;
    }

    public String getUNLIMETEDUSER() {
        return mUNLIMETEDUSER;
    }

    public void setUNLIMETEDUSER(String uNLIMETEDUSER) {
        mUNLIMETEDUSER = uNLIMETEDUSER;
    }

    @NonNull
    @Override
    public String toString() {
        return PrefUtils.getInstance().getAppLanguage().equals("ar") ? mEMPARNAME : mEMPENNAME;
    }
}
