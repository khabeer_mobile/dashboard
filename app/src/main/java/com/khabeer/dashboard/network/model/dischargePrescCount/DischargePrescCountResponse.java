package com.khabeer.dashboard.network.model.dischargePrescCount;

import com.google.gson.annotations.SerializedName;
import com.khabeer.dashboard.network.model.BaseResponse;

public class DischargePrescCountResponse extends BaseResponse {

	@SerializedName("DISCHARGE_PRESC_STAT_ROW")
	private DischargePrescStatRow dischargePrescStatRow;

	public DischargePrescStatRow getDischargePrescStatRow() {
		return dischargePrescStatRow;
	}
}