package com.khabeer.dashboard.network.model.dischargePrescCount;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.khabeer.dashboard.network.model.BaseResponse;
import com.khabeer.dashboard.utils.PrefUtils;

public class DischargePrescStatRow extends BaseResponse {

	@SerializedName("STATUS_NAME_AR")
	private String statusNameAr;

	@SerializedName("RESULT_COUNT")
	private String resultCount;

	@SerializedName("ID")
	private String id;

	@SerializedName("STATUS_NAME_EN")
	private String statusNameEn;

	@NonNull
	@Override
	public String toString() {
		return PrefUtils.getInstance().getAppLanguage().equals("ar") ? statusNameAr : statusNameEn;
	}
}